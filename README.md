# Zadanie pre účely prijímacieho pohovoru #

Na tejto úlohe si otestujem všetko, čo potrebujem o tebe vedieť a budem sa vedieť rozhodnúť, či
možná spolupráca s tebou bude pre mňa prínos alebo nie.

Tvojou úlohou je napísať jednoduchý software pre monitorovanie denných aktivít na hostingových servroch a poskytovať
o nich rovnako raz denne mailom report.


### Požiadavky na aplikáciu ###

Aplikácia sa skladá z dvoch častí, serverovej a klientskej. Na viacerých hostingových servroch prebiehajú rôzne
úlohy, ktoré zabezpečujú správny beh daného servra. Je dôležité, aby tieto úlohy každý deň prebehli a ich prípadné
zlyhanie musí byť okamžite podchytené a vyriešené, aby sa predišlo problémom.

Mechanizmus je jednoduchý. Serverovská časť pozostáva z jednoduchého modelu, ktorý už je čiastočne predpripravený
a definuje servre a služby na nich bežiace. Úlohy sa na servroch púšťajú v crone a cez piping sa výstup z nich
odošle do jednoduchého API na servri. Server výstup z danej úlohy spracuje, porovná s databázovou definíciou
a pomocou REGEX výrazu vyhodnotí, či úloha zbehla korektne.

Na servrovskej časti je potrebné dopracovať model, doplniť ho o formu logu, kde sa bude zachytávať vstup z klienta cez
API. Server v definovaný čas raz denne log vyhodnotí a odošle report na email, kde sumárne zobrazí všetky
úlohy a ich stav (OK, FAILED, MISSING).

Ukážka pipingu klientskej časti aplikácie, ktorá prevezme výstup z úlohy a odošle ho do servrovského API:

    $ duply sauron backup | vemos.py duply


Klientská časť bude Django management command, teda celá aplikácia sa bude inštalovať na cieľové servre, ale bude sa
využívať len tento command, ktorý spracuje výstup príkazu cez piping a odošle output do HTTP RESTful API servra
s použitím [requests](http://docs.python-requests.org/en/latest/)


### Postup prác ###

* Vytvor si konto na bitbucket
* Forkni toto repository a svoj vývoj rieš vo svojom vlastnom repository
* Commituj často a priebežne, aby som videl priebeh tvojho vývoja

