from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from models import Log, Server, Type


@csrf_exempt
@require_POST
def index(request):
    server = Server.objects.filter(name=request.POST['serverName'])
    type = Type.objects.filter(name=request.POST['type'])

    if server and type:
        log = Log(server=request.POST['serverName'], type=request.POST['type'], is_missing=False, output=request.POST['output'])
        print log.server + log.type + log.output

    else:
        log = Log(server=request.POST['serverName'], type=request.POST['type'], is_missing=True, output=request.POST['output'])

    log.save()
    return HttpResponse('spracovane')