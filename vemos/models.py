from django.db import models


class Server(models.Model):
    name = models.CharField(max_length=40, unique=True)
    description = models.TextField(blank=True)
    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.name


class Type(models.Model):
    name = models.CharField(max_length=20, unique=True)
    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.name


class Service(models.Model):
    server = models.ForeignKey(Server)
    type = models.ForeignKey(Type)
    is_active = models.BooleanField()
    validation_regex_test = models.CharField(max_length=200)
    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return '%s (%s)' % (self.server.name, self.type.name)

class Log(models.Model):
    #service = models.ForeignKey(Service)
    server = models.CharField(max_length=40)
    type = models.CharField(max_length=20)
    is_missing = models.BooleanField()
    output = models.CharField(max_length=500)
    created = models.DateTimeField(auto_now_add=True)