import requests
import sys
import socket

__author__ = 'marek'


from optparse import make_option
from django.core.management.base import BaseCommand, CommandError

class Command(BaseCommand):
    help = 'Help text goes here'

    def handle(self, *args, **options):

        if args:
            output = ''
            for line in sys.stdin:
                output += line
            info = {'serverName': socket.gethostname(), 'type': args[0], 'output': output}
            r = requests.post("http://localhost:8000", data=info)
            print r
        else: print 'argument missing'