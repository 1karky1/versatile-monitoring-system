import re
from vemos.models import Log, Server, Service, Type
from django.utils import timezone
from django.core.mail import send_mail

__author__ = 'marek'


from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Help text goes here'

    def handle(self, *args, **options):
        end_time = timezone.now()
        start_time = end_time - timezone.timedelta(hours=12)
        logs = Log.objects.filter(created__range=(start_time,end_time))
        message = ''

        for log in logs:
            message += 'Server name: '+log.server+'\tType: '+log.type+'\t'

            if log.is_missing:
                message += 'Status: MISSING\n'

            else:
                server = Server.objects.get(name=log.server)
                type = Type.objects.get(name=log.type)
                service = Service.objects.get(server=server, type=type)
                a = re.compile(service.validation_regex_test)

                if a.match(log.output):
                    message += 'Status: OK\n'

                else:
                    message += 'Status: FAILED\n'

        #print message
        send_mail('Vemos kontrola logu', message, 'from@example.com',
                  ['to@example.com'], fail_silently=False)